

# Source code for the OnTheGo Auto Spares web app.


## Installation

1. Inside the directory type `composer install`
1. Copy `.env.example` file to `.env`
1. `php artisan key:generate`
1. Set database credentials in  `.env` file
1. Set Stripe credentials in `.env` file. Specifically `STRIPE_KEY` and `STRIPE_SECRET`
1. Set Algolia credentials in `.env` file. Specifically `ALGOLIA_APP_ID` and `ALGOLIA_SECRET`
1. Set your Braintree credentials in your `.env` file if you want to use PayPal. Specifically `BT_MERCHANT_ID`, `BT_PUBLIC_KEY`, `BT_PRIVATE_KEY`.
1. `php artisan ecommerce:install`. This will migrate the database and run any seeders necessary. 
1. `npm install`
1. `npm run dev`
1. `php artisan serve` or use Laravel Valet or Laravel Homestead
1. Visit `localhost:8000` in your browser
1. Visit `/admin` if you want to access the Voyager admin backend. Admin User/Password: `admin@admin.com/password`. Admin Web User/Password: `adminweb@adminweb.com/password`

## Shopping Cart Package
[hardevine/LaravelShoppingcart](https://github.com/hardevine/LaravelShoppingcart) which is a forked version that updates quicker.

## On Windows there is a money_format() issue

The `money_format` function does not work in Windows. Take a look at [this thread](https://stackoverflow.com/questions/6369887/alternative-to-money-format-function-in-php-on-windows-platform/18990145). As an alternative, just use the `number_format` function instead.

1. In `app/helpers.php` replace `money_format` line with `return '$'.number_format($price / 100, 2);`
1. In `app/Product.php` replace `money_format` line with `return '$'.number_format($this->price / 100, 2);`
1. In `config/cart.php` set the `thousand_seperator` to an empty string or you might get a 'non well formed numeric value encountered' error. It conflicts with `number_format`.


