<footer>
    <div class="footer-content container">
        <div class="made-with">On The Go - Spares. &copy 2020. All rights researved. </div>
        {{ menu('footer', 'partials.menus.footer') }}
    </div> <!-- end footer-content -->
</footer>
