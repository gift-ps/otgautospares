<?php

Route::get('/', 'LandingPageController@index')->name('landing-page');

Route::get('/shop', 'ShopController@index')->name('shop.index');
Route::get('/shop/{product}', 'ShopController@show')->name('shop.show');

Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart/{product}', 'CartController@store')->name('cart.store');
Route::patch('/cart/{product}', 'CartController@update')->name('cart.update');
Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');
Route::post('/cart/switchToSaveForLater/{product}', 'CartController@switchToSaveForLater')->name('cart.switchToSaveForLater');

Route::delete('/saveForLater/{product}', 'SaveForLaterController@destroy')->name('saveForLater.destroy');
Route::post('/saveForLater/switchToCart/{product}', 'SaveForLaterController@switchToCart')->name('saveForLater.switchToCart');

Route::post('/coupon', 'CouponsController@store')->name('coupon.store');
Route::delete('/coupon', 'CouponsController@destroy')->name('coupon.destroy');

Route::get('/checkout', 'CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::post('/checkout', 'CheckoutController@store')->name('checkout.store');
Route::post('/paypal-checkout', 'CheckoutController@paypalCheckout')->name('checkout.paypal');
Route::post('/momo-checkout', 'CheckoutController@momoCheckout')->name('checkout.momo');

//Guest-Step 1🚶🏾‍♂ gets user billing forms. route comes from cart🔀
Route::get('/add-billing-infomation', 'CheckoutController@userBilling')->name('billing');
//Guest-Step 2. if user selects guest mode, they come to this route. a guest form awaits them😊😊
//It returns the (guest-billing) view. A POST from here goes to billingDetails
//This 👇🏾 is the custome one - shows guest as guest. It saves data and get (id) to guestCheckout
Route::get('/add-biiling-infomation-guest', 'CheckoutController@guestBilling1')->name('checkout.guest');
//Get billing details from both user and guest.
Route::post('/finish-billing', 'CheckoutController@billingDetails')->name('billingDetails');
//checkout user and guest
Route::get('/userCheckout/{id}', 'CheckoutController@index')->name('userCheckout')->middleware('auth');
//Guest-Step 3. Choose a payment method and make payment💸💰🤑
//Use the (id) carried from guestBilling1 to edit/update Order and the geteway specific data
//If successful, thank🙏🏾 guest/user at confirmation.index page.	DONE✅💚✅
Route::get('/complete-checkout/{id}', 'CheckoutController@index')->name('guestCheckout');

Route::get('/thankyou', 'ConfirmationController@index')->name('confirmation.index');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/search', 'ShopController@search')->name('search');

Route::get('/search-algolia', 'ShopController@searchAlgolia')->name('search-algolia');

Route::middleware('auth')->group(function () {
    Route::get('/my-profile', 'UsersController@edit')->name('users.edit');
    Route::patch('/my-profile', 'UsersController@update')->name('users.update');

    Route::get('/my-orders', 'OrdersController@index')->name('orders.index');
    Route::get('/my-orders/{order}', 'OrdersController@show')->name('orders.show');
});

